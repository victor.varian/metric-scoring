## Metric Scoring
Purpose: Need to know which evaluation metrics that will give a score that is as close as human evaluation

Overview of this repo is explained in this google slides:
https://drive.google.com/file/d/1P86rCD_QeesXzrcH3mXFkBOPQ5ta4Fjs/view?usp=sharing

<br>

### About this repository folder
**`final_data`**: list of csv for COMET Estimator and COMET Ranker training dataset with reference score can be any of 3 or 4

**`modified_top_data_only`**: list of csv for COMET Estimator and COMET Ranker training dataset with reference score can only be of top value only (one of 3 or 4)

```
Example:
Given translation score: [4,3,2,1]

Dataset of [4,3,1], [4,2,1] will be included in csv of folder modified_top_data_only but [3,2,1] isn't included.
```

**`scoring_data`**: list of all collected data per language pair from shopee UAT folder. There is also a schema under this folder. You can also view the schema here: https://docs.google.com/spreadsheets/d/13CE5pcWz7QC1PRuv01YMwrfgWw2Ip9BQb4Vrhlywhfc/edit?usp=sharing

**`COMET_Estimator_Extraction.ipynb`**: Follow this notebook to get the extracted csv shown in the folder `final_data` or `modified_top_data_only`

**`COMET_Ranker_Extraction.ipynb`**: Follow this notebook to get the extracted csv shown in the folder `final_data`

**`comet_estimator_configs.yaml`**: configurations used for training COMET estimator model

**`comet_ranker_configs.yaml`**: configurations used for training COMET ranker model

COMET Estimator and COMET Ranker term are based from this paper: https://www.aclweb.org/anthology/2020.emnlp-main.213.pdf

<br>

**`model_evaluation.ipynb`**: Follow this notebook to evaluate your trained model or traditional MT evaluation model with the test set from `final_data` folder

We have tested our model for BLEU, CHRF, BERT SCORE on a larger dataset here: https://docs.google.com/spreadsheets/d/1JFsSvZm7dTedtvhxhSLmQpwhkxGAtYsV-kIJzvcZ_5c/edit?usp=sharing

We have tested our model for BLEU, CHRF, COMET Estimator, COMET Ranker, BERT SCORE on a smaller dataset here: https://docs.google.com/spreadsheets/d/1AkCA-9RTagBL_W2UMwZQsZdIoYNxV13T7WE9-o-y43E/edit?usp=sharing


### Additional things to note:
I am using unbabel-comet==0.1.0 for the COMET version. Most likely when you want to train your ranker model, you may encounter some error on undefined `config.hparam.layer`. If so, clone the github repo and just replace this value with hard value that you want, then pip install again. 

